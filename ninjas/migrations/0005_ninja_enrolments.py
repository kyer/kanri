# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0005_auto_20150314_0848'),
        ('ninjas', '0004_auto_20150319_1616'),
    ]

    operations = [
        migrations.AddField(
            model_name='ninja',
            name='enrolments',
            field=models.ManyToManyField(help_text=b'Dojo terms the Ninja has enroled in.', to='dojo.Term'),
            preserve_default=True,
        ),
    ]
