# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ninjas', '0006_auto_20150322_1533'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ninja',
            old_name='applied_terms',
            new_name='applied_ninjas',
        ),
        migrations.RenameField(
            model_name='ninja',
            old_name='enrolled_terms',
            new_name='enrolled_ninjas',
        ),
    ]
