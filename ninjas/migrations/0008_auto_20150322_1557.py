# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ninjas', '0007_auto_20150322_1550'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ninja',
            old_name='applied_ninjas',
            new_name='applied_dojos',
        ),
        migrations.RenameField(
            model_name='ninja',
            old_name='enrolled_ninjas',
            new_name='enrolled_dojos',
        ),
    ]
