# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ninjas', '0003_auto_20150319_1432'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ninja',
            name='age',
        ),
        migrations.AddField(
            model_name='ninja',
            name='school_year',
            field=models.PositiveSmallIntegerField(default=1, help_text=b"The ninja's school year."),
            preserve_default=False,
        ),
    ]
