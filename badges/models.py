from django.db import models


class Link(models.Model):
    name = models.CharField(
        max_length=50,
        unique=True,
        help_text="The link's human-readable name."
    )

    note = models.CharField(
        max_length=100,
        blank=True,
        help_text='An optional note about the link.'
    )

    url = models.URLField(
        blank=False,
        help_text="The URL."
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Category(models.Model):
    name = models.CharField(
        max_length=50,
        unique=True,
        help_text="The name of the category."
    )

    slug = models.SlugField(
        max_length=15,
        unique=True,
        help_text="A unique slug for the category."
    )

    image = models.ImageField(
        help_text="The category icon. Square transparent PNGs please.",
        blank=True,
    )

    links = models.ManyToManyField(
        'badges.Link',
        blank=True,
        help_text='A collection of relevant links (e.g. Codecademy '
                  'track URLs).'
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Badge(models.Model):
    name = models.CharField(
        max_length=30,
        unique=True,
        help_text="The badge's visible name"
    )

    slug = models.SlugField(
        max_length=30,
        unique=True,
        help_text="A unique slug for the badge."
    )

    image = models.ImageField(
        help_text="The badge icon. Square transparent PNGs please.",
        blank=True,
    )

    short_description = models.CharField(
        max_length=75,
        help_text="A short description of what this badge means, in the "
                  "past tense (e.g. this badge shows that you have...)"
    )

    requirements = models.TextField(
        max_length=4096,
        help_text="In-depth requirements. What do you need "
                  "to do to get this badge?"
    )

    order = models.PositiveSmallIntegerField(
        help_text='The order in which this badge will appear amongst other '
                  'badges in a list.',
        blank=False
    )

    categories = models.ManyToManyField(
        'badges.Category',
        help_text='The categories this badge belongs to.'
    )

    recipients = models.ManyToManyField(
        'ninjas.Ninja',
        through='badges.BadgeAward',
        help_text='The Ninjas that have received the badge.'
    )

    attachments = models.ManyToManyField(
        'badges.Attachment',
        blank=True,
        help_text='A collection of relevant attachments (e.g. worksheets).'
    )

    links = models.ManyToManyField(
        'badges.Link',
        blank=True,
        help_text='A collection of relevant links (e.g. Codecademy '
                  'track URLs).'
    )

    @property
    def relevant_links(self):
        return (Link.objects.filter(category=self.categories.all())
                | self.links.all())

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['order']


class Attachment(models.Model):
    name = models.CharField(
        max_length=50,
        unique=True,
        help_text="The attachment's human-readable name."
    )

    attachment = models.FileField(
        help_text="The attachment. Nothing too big, please!",
        blank=False
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class BadgeAward(models.Model):
    timestamp = models.DateTimeField(
        auto_now_add=True,
        help_text="The time at which the badge was awarded."
    )

    notes = models.TextField(
        blank=True,
        help_text='Optional notes regarding the awarding of this '
                  'badge to this ninja.'
    )

    badge = models.ForeignKey(
        'badges.Badge',
        help_text='The badge to award.'
    )

    ninja = models.ForeignKey(
        'ninjas.Ninja',
        help_text='The Ninja to award the badge to.'
    )

    awarder = models.ForeignKey(
        'mentors.Mentor',
        help_text='The Mentor that awarded the badge to this Ninja.'
    )

    class Meta:
        unique_together = ['badge', 'ninja']
        ordering = ['-timestamp', 'ninja__user__first_name']
