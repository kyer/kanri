from django.contrib import admin
import models


@admin.register(models.Badge)
class BadgeAdmin(admin.ModelAdmin):
    list_display = ['name', 'short_description', 'order']
    list_filter = ['categories']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(models.BadgeAward)
class BadgeAwardAdmin(admin.ModelAdmin):
    list_display = ['badge', 'ninja', 'awarder', 'timestamp']


@admin.register(models.Attachment)
class AttachmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'attachment']


@admin.register(models.Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ['name', 'url']
