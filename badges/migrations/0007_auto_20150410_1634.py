# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0006_auto_20150410_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='slug',
            field=models.SlugField(default='no-slug', help_text=b'A unique slug for the badge.', unique=True, max_length=15),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='slug',
            field=models.SlugField(default='no-slug', help_text=b'A unique slug for the category.', unique=True, max_length=15),
            preserve_default=False,
        ),
    ]
