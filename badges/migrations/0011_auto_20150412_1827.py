# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0010_auto_20150412_1801'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='badgeaward',
            options={'ordering': ['-timestamp', 'ninja__user__first_name']},
        ),
    ]
