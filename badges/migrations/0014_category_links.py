# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0013_auto_20150412_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='links',
            field=models.ManyToManyField(help_text=b'A collection of relevant links (e.g. Codecademy track URLs).', to='badges.Link', blank=True),
            preserve_default=True,
        ),
    ]
