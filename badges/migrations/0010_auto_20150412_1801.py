# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0009_auto_20150411_0856'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='badgeaward',
            unique_together=set([('badge', 'ninja')]),
        ),
    ]
