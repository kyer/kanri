# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0005_auto_20150410_1507'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='badge',
            options={'ordering': ['order']},
        ),
        migrations.AddField(
            model_name='badge',
            name='order',
            field=models.PositiveSmallIntegerField(default=1, help_text=b'The order in which this badge will appear amongst other badges in a list.'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='badge',
            name='short_description',
            field=models.CharField(help_text=b'A short description of what this badge means, in the past tense (e.g. this badge shows that you have...)', max_length=75),
            preserve_default=True,
        ),
    ]
