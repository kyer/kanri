# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0012_auto_20150412_1845'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The link's human-readable name.", unique=True, max_length=50)),
                ('url', models.URLField(help_text=b'The URL.')),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='badge',
            name='links',
            field=models.ManyToManyField(help_text=b'A collection of relevant links (e.g. Codecademy track URLs).', to='badges.Link', blank=True),
            preserve_default=True,
        ),
    ]
