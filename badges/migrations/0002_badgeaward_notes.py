# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='badgeaward',
            name='notes',
            field=models.TextField(help_text=b'Optional notes regarding the awarding of this badge to this ninja.', blank=True),
            preserve_default=True,
        ),
    ]
