# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0008_auto_20150410_2136'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The attachment's human-readable name.", max_length=50)),
                ('attachment', models.FileField(help_text=b'The attachment. Nothing too big, please!', upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='badge',
            name='attachments',
            field=models.ManyToManyField(help_text=b'A collection of relevant attachments (e.g. worksheets).', to='badges.Attachment', blank=True),
            preserve_default=True,
        ),
    ]
