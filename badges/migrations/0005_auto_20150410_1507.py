# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0004_auto_20150410_1411'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of the category.', max_length=50)),
                ('image', models.ImageField(help_text=b'The category icon. Square transparent PNGs please.', upload_to=b'', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='badge',
            name='categories',
            field=models.ManyToManyField(help_text=b'The categories this badge belongs to.', to='badges.Category'),
            preserve_default=True,
        ),
    ]
