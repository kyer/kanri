# from django.shortcuts import render
from django.views import generic
import models
import ninjas


class BadgeListView(generic.ListView):
    model = models.Category
    template_name = 'badges/list.html'


class BadgeLeaderboard(generic.ListView):
    model = ninjas.models.Ninja
    template_name = 'badges/leaderboard.html'


class AwardTimeline(generic.ListView):
    model = models.BadgeAward
    template_name = 'badges/timeline.html'
