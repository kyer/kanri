from django.contrib import admin

import models


@admin.register(models.Dojo)
class DojoAdmin(admin.ModelAdmin):
    list_display = ['name', 'location']


@admin.register(models.Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'start', 'finish', 'session_type', 'term']
    list_filter = ['term', 'session_type']


@admin.register(models.Term)
class TermAdmin(admin.ModelAdmin):
    list_display = ['name', 'dojo']


@admin.register(models.Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ['name', 'description']
