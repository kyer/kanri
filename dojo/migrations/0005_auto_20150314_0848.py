# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0004_auto_20150314_0753'),
    ]

    operations = [
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The role's name.", max_length=40)),
                ('description', models.TextField(help_text=b'A brief description of the role.', blank=True)),
                ('dojo', models.ForeignKey(help_text=b'The Dojo that the role is relevant to.', to='dojo.Dojo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='role',
            unique_together=set([('name', 'dojo')]),
        ),
    ]
