# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0007_role_advertised'),
    ]

    operations = [
        migrations.AlterField(
            model_name='role',
            name='advertised',
            field=models.BooleanField(default=False, help_text=b'Should this role be advertised to mentors?'),
            preserve_default=True,
        ),
    ]
