# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0006_auto_20150322_1046'),
    ]

    operations = [
        migrations.AddField(
            model_name='role',
            name='advertised',
            field=models.BooleanField(default=True, help_text=b'Should this role be advertised to mentors?'),
            preserve_default=False,
        ),
    ]
