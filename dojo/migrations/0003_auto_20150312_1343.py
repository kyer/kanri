# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0002_term'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='session',
            name='dojo',
        ),
        migrations.AddField(
            model_name='session',
            name='term',
            field=models.ForeignKey(default=1, to='dojo.Term', help_text=b'The term during which this session will run.'),
            preserve_default=False,
        ),
    ]
