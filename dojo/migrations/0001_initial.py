# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dojo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The Dojo's unique, human-readable name.", unique=True, max_length=50)),
                ('location', models.TextField(help_text=b"The dojo's physical location.")),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of the session.', max_length=50, blank=True)),
                ('description', models.TextField(help_text=b'A short description of the session.', blank=True)),
                ('start', models.DateTimeField(help_text=b'The starting date/time of the session.')),
                ('finish', models.DateTimeField(help_text=b'The finishing date/time of the session.')),
                ('session_type', models.CharField(help_text=b'The type of session being run.', max_length=3, choices=[(b'REG', b'Regular Dojo'), (b'TRN', b'Mentor Training Session'), (b'MET', b'Planning/Meeting Session'), (b'SOC', b'Social Event')])),
                ('dojo', models.ForeignKey(help_text=b'The Dojo at which this session will run.', to='dojo.Dojo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
