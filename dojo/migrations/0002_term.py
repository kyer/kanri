# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Term',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of the term (e.g. Semester 1, 2015).', max_length=50)),
                ('dojo', models.ForeignKey(help_text=b'The Dojo this term is running at.', to='dojo.Dojo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
