from django.db import models


class Term(models.Model):
    name = models.CharField(
        max_length=50,
        help_text="The name of the term (e.g. Semester 1, 2015)."
    )

    dojo = models.ForeignKey(
        'dojo.Dojo',
        help_text='The Dojo this term is running at.'
    )

    def __str__(self):
        return self.name
