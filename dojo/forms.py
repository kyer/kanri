from django import forms

import models


class RoleSelectForm(forms.Form):
    role = forms.ModelChoiceField(
        queryset=models.Role.objects.all()
    )
