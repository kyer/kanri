# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0002_auto_20150311_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mentor',
            name='mobile',
            field=models.CharField(help_text=b"The mentor's mobile contact number.", max_length=10, validators=[django.core.validators.RegexValidator(regex=b'^0\\d{9}$', message=b'Not a valid mobile phone number.')]),
            preserve_default=True,
        ),
    ]
