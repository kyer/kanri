# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0002_term'),
        ('mentors', '0007_auto_20150312_1037'),
    ]

    operations = [
        migrations.AddField(
            model_name='mentor',
            name='enrolments',
            field=models.ManyToManyField(help_text=b'The Dojos this mentor is enroled to help in.', to='dojo.Dojo', blank=True),
            preserve_default=True,
        ),
    ]
