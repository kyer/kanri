# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0013_auto_20150406_1939'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mentor',
            options={'ordering': ['user__first_name']},
        ),
    ]
