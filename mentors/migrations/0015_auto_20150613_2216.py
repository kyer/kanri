# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0014_auto_20150412_1810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mentor',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL, help_text=b'The user account to associate with this mentor.'),
        ),
    ]
