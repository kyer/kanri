from django.contrib import admin

import models


@admin.register(models.Mentor)
class MentorAdmin(admin.ModelAdmin):
    list_display = ['user', 'gender', 'mobile']
    list_filter = ['gender']
