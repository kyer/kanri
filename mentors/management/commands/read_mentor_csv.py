from django.core.management import base
from django.contrib import auth

from mentors.models import Mentor

import csv


class Command(base.BaseCommand):
    """Import a Google Forms-generated Mentor sign-up CSV.

    The CSV's format is hard-coded. I don't really have plans to fix this
    limitation because I never actually know what the next term's form will
    look like.

    This command takes a list of paths to CSV files, and imports all rows.
    """
    def handle(self, *args, **options):
        for path in args:
            try:
                csv_file = open(path, 'rb')
                csv_reader = csv.DictReader(csv_file)
            except:
                raise base.CommandError("Issues with File I/O: %s" % csv_file)
            for row in csv_reader:
                self.process_row(row)

    def process_row(self, row):
        """
        Process a single CSV row, generating the necessary database
        objects.
        """
        # First, try to match the Mentor with an existing user.
        # This should be easy to do with the email address.
        try:
            user = auth.get_user_model().objects.get(email=row['Email']
                                                     .strip())
        except:
            # No user found. Create a new one from the information we have.
            user = auth.get_user_model()()
            user.first_name = row['First Name'].strip()
            user.last_name = row['Family name'].strip()
            user.email = row['Email'].strip()
            user.username = ("{u.first_name}.{u.last_name}".format(u=user)
                             .lower())

            # The account will be pseudo-locked by default.
            user.set_unusable_password()
            user.full_clean()
            user.save()

        # Add the user to the Mentors group.
        user.groups.add(auth.models.Group.objects.get(name='Mentors'))

        # Check if the user has an associated Mentor object.
        try:
            mentor = Mentor.objects.get(user=user)
        except:
            mentor = Mentor()

        # Even if the mentor record already exists, we update it with the
        # newest information.
        mentor.user = user
        if row['Gender'][0] in ('M', 'F'):
            mentor.gender = row['Gender'][0]  # M = Male, F = Female.etc
        else:
            mentor.gender = 'O'
        mentor.mobile = row['Mobile ']

        mentor.full_clean()
        mentor.save()
