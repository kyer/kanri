cheet('↑ ↑ ↓ ↓ ← → ← → b a', {
    next: function (str, key, num, seq) {
        console.log('key pressed: ' + key);
        console.log('progress: ' + num / seq.length);
        console.log('seq: ' + seq.join(' '));
    },
    
    fail: function () {
        console.log('sequence failed');
    },
    
    done: function () {
        $('#konami').removeClass('hidden')
        $('#brand_text').addClass('hidden')
        $('#brand_logo').removeClass('hidden')
        $('#navbar').addClass('bgflash')
    }
});

function showCredits(){
    $("#footer_logo").addClass("hidden");
    $("#footer_credits").removeClass("hidden");
}

function hideCredits(){
    $("#footer_logo").removeClass("hidden");
    $("#footer_credits").addClass("hidden");
}