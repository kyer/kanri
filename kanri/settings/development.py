from __future__ import unicode_literals, absolute_imports
from .base import *
import os


SECRET_KEY = os.environ.get('KANRI_SECRET')

DEBUG = False
TEMPLATE_DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('KANRI_DB_BACKEND'),
        'NAME': os.environ.get('KANRI_DB_NAME'),
        'USER': os.environ.get('KANRI_DB_USER'),
        'PASSWORD': os.environ.get('KANRI_DB_PASSWORD'),
        'HOST': os.environ.get('KANRI_DB_HOST'),
        'PORT': os.environ.get('KANRI_DB_PORT'),
    }
}
