# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0006_auto_20150311_1506'),
        ('dojo', '0001_initial'),
        ('ninjas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attendance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arrival', models.DateTimeField(help_text=b'When did the ninja arrive?')),
                ('ninja', models.ForeignKey(help_text=b'What ninjas is attending?', to='ninjas.Ninja')),
                ('session', models.ForeignKey(help_text=b'What session is the Ninja attending?', to='dojo.Session')),
                ('sighted_by', models.ForeignKey(help_text=b'Which mentor sighted this Ninja upon arrival and ensured their safety and supervision?', to='mentors.Mentor')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
