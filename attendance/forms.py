from django import forms

import models


class AttendanceAjaxForm(forms.ModelForm):
    here = forms.BooleanField(
        required=False
    )

    class Meta:
        model = models.Attendance
        fields = ['ninja', 'session']
