from fabric.api import *


def run(command='check'):
    local("./manage.py {command}".format(command=command))
